# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summaryr
This Repository comprises of a very basic console application that reads a set of Word Documents and appends them using 
Alt-Soft to create a PDF based on given parameters.

There is a separate folder named 'WordDocs' that consist of the set of Word Documents to be processed for testing the application.

Copy the folder to any local drive and update the location in the code section where it accessed the folder.

You will notice that once the console application runs, it processes the first 2 or 3 documents are then starts 
kicking exceptions that the file format is wrong.

If the same process is run using MemoryStream, you will not see the problem. But we are trying to implement FileStream as 
it generates PDF's that are 4-10 times smaller than what MemoryStream generates. We want a fix for the issue while using
'FileStream'.

### How do I get set up? ###

You can add this repository in Visual Studio by clicking the 'Connect to Team Projects' button and 
then under 'Local Git Repositories', click 'Clone' and paste this :
' https://victorcchinta@bitbucket.org/victorcchinta/altsoftconsole.git ' and set a destination path and click 'Clone'.

You can also install 'SourceTree' and get the repository to your local through it.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact