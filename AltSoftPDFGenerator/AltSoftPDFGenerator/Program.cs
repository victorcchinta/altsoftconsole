﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Altsoft.Publisher;

namespace AltSoftPDFGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            LetterExtractor();
        }

        public static void LetterExtractor()
        {
            try
            {
                
            APEngine engine = new APEngine(false);

            // Update Alt-Soft License ID in the App.config 
            engine.Load(ConfigurationManager.AppSettings["AltSoftLicense"]);

            APSourceFormat srcFormat = APSourceFormat.Unknown;
            APDestinationFormat destFormat = APDestinationFormat.PDF;

            var itemsPerFile = Int32.Parse(ConfigurationManager.AppSettings["LetterFileMaxItemCount"]);


            // Reading Files from local
            Dictionary<int, byte[]> letters = new Dictionary<int, byte[]>();
            int ltrId = 1;

            // Update the URL to the location you place the Word Document folder at!
            foreach (string file in Directory.GetFiles(@"C:\temp\WordDoc\", "*.docx"))
            {
                byte[] letterBytes = System.IO.File.ReadAllBytes(file);
                letters.Add(ltrId, letterBytes);
                ltrId++;
            }

            var sequenceCount = letters.Count / itemsPerFile + (letters.Count % itemsPerFile > 0 ? 1 : 0);
            var indx = 0;
            var batchCount = 0;
            int batchID = 1;
            string pdfFilePath = "C:/temp/";

            // Collecting the List of Letters kicked out as Exception - Unsupported Format issue 
            Dictionary<int, byte[]> lettersWithMemoryIssues = new Dictionary<int, byte[]>();

            for (int sequenceIndex = 0; sequenceIndex < sequenceCount; ++sequenceIndex)
            {
                APLog log = new APLog();

                //Initialize variables to check the pagecount and Letter count for each batch count
                var filePageCount = 0;
                var lettersCount = 0;
                ++batchCount; // Understand the concept of batch here??

                #region PDF File Naming

                var destinationFileName = String.Empty;
                string batchId = Convert.ToString(batchID);
                string strEnvelopeSize = String.Empty;
                batchId = batchId.PadLeft(batchId.Length + (10 - batchId.Length), '0');

                destinationFileName = string.Format("Novologix_{0}_print{1}.PDF",
                    System.DateTime.Now.AddDays(-1).ToString("yyyyMMddHHmm"), (sequenceIndex + 1).ToString("000")
                );

                #endregion


                using (APDestination dest = engine.CreateDestination(engine.CreateConfig()))
                {
                    var letterList = letters.Skip(sequenceIndex * itemsPerFile).Take(itemsPerFile).ToList();
                    foreach (var l in letterList)
                    {
                       // The following process works when using Memory Stream is being used, but has problems when using FileStream
 
                       //using (MemoryStream sourcestream = new MemoryStream(l.Value))
                        using (FileStream sourcestream = new FileStream(destinationFileName, FileMode.Create))
                        {
                           
                            sourcestream.Write(l.Value, 0, l.Value.Length); // Comment this if you are using MemoryStream
                            sourcestream.Position = 0;

                            using (APSource src = engine.CreateSource(sourcestream, "", srcFormat, false))
                                try
                                {
                                    // For every Exception, dest.PageCount gets 1 empty page added!! 
                                    int previousCount = dest.PageCount;
                                    dest.Append(src, log, 0.4);
                                    ++lettersCount;
                                    filePageCount = dest.PageCount - previousCount;

                                }
                                catch (Exception ex)
                                {
                                    lettersWithMemoryIssues.Add(l.Key, l.Value);


                                }

                        }
                    }
                    dest.Save(destFormat, Path.Combine(pdfFilePath, destinationFileName), log, 0.2);
                }

            }
        }

            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
